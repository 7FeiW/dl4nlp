from typing import Tuple

import numpy as np
import pickle
import matplotlib.pyplot as plt

# This is over engineering , but I do like sklearn style api
class LogisticRegression:
    
    @classmethod
    def signmoid(cls, z:np.ndarray) -> np.ndarray:
        """
        Method of sigmoid function
        Args:
            z (np.ndarray): z values

        Returns:
            np.ndarray: output of sigmoid function
        """        
        return 1 / ( 1 + np.exp(-z))
    
    @classmethod
    def get_cost(cls, y:np.ndarray, predicted_y_value:np.ndarray) -> float:
        """Method to compute cost 
        Args:
            predicted_y_value(np.ndarray): [description]
            y (np.ndarray): [description]

        Returns:
            float: cost 
        """
        
        # cost function
        # logistic regression has two part of costs, y = 1 and y = 0 
        # y = -log(y_predicted) if y = 1 , y = -log(1 - y_predicted) if y = 0
        # reference https://ml-cheatsheet.readthedocs.io/en/latest/logistic_regression.html#cost-function
        positive_cases = - y * np.log(predicted_y_value)
        negative_cases = - (1 - y ) * np.log(1 - predicted_y_value)
        cost = (positive_cases + negative_cases).mean()
        return cost
        
    @classmethod
    def get_gradient(cls, X:np.ndarray, y:np.ndarray, weights:np.ndarray) -> np.ndarray:
        """
        Method to compute cost and gradient, return gradients and cost
        Args:
            X (np.ndarray): Training features, num_sample by num_features.
            y (np.ndarray): Training label, num_sample by 1.
            weights (np.ndarray): weights for this model

        Returns:
           np.ndarray: gradients
        """
        # (sample, feature) .* (feature,1)  
        z  = np.dot(X, weights) # z is (sample_name ,1) matrix
        predicted_y_value= cls.signmoid(z) # predicted_y_valueis (sample_name ,1) matrix

        # run gradient descent
        # compute loss
        loss = predicted_y_value- y  # loss is predicted_y_value(sample_name ,1) matrix
        num_samples = X.shape[0]
        # (sample, feature).T  .* (sample_name ,1)
        # for math referrence https://ml-cheatsheet.readthedocs.io/en/latest/logistic_regression.html#gradient-descent
        gradients = np.dot(X.T, loss) / num_samples
        
        return gradients
    
                
    @classmethod     
    def train(cls, X: np.ndarray, y:np.ndarray, X_val: np.ndarray, y_val:np.ndarray, learning_rate = 0.1, 
        num_epochs = 300, batch_size = 20, add_bias:bool = True) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """
        Method to train the model
        args:
            X (np.ndarray): Training features, num_sample by num_features.
            y (np.ndarray): Training label, num_sample by 1.
            X_val (np.ndarray): Validation features, num_sample by num_features.
            y_val (np.ndarray): Validation label, num_sample by 1.
            learning_rate (float, optional): learning rate. Defaults to 0.1.
            num_epochs (int, optional): num of epoches. Defaults to 300.
            batch_size (int, optional): size of batch. Defaults to 20.
            add_bias (bool, optional): boolean value to add bias, if true, 1 is added in front of each row for bias. Defaults to True.

        Returns:
            Tuple[np.ndarray, np.ndarray, np.ndarray]: weights, list of training accuarcy, and list of validation accuarcy
        """

        # init features, with bias term
        features =  X if not add_bias else np.hstack((np.ones((X.shape[0], 1)), X))
        # weights is predicted_y_value(feature + 1, 1) matrix
        weights = np.random.uniform(low = -0.5, high = 0.5, size = (features.shape[1], 1))
        
        # init some vals
        best_model_weights = None 
        best_accuacy = 0.0
        training_accuracy_hist = np.zeros(num_epochs)
        validation_accuracy_hist = np.zeros(num_epochs)
        
        for epoch in range(num_epochs):
            row_idx = 0
            while row_idx < features.shape[0]:
                upper_bound = row_idx + batch_size if row_idx + batch_size < features.shape[0] else features.shape[0] -1
                # compute and gradients
                gradients = cls.get_gradient(features[row_idx: upper_bound], y[row_idx: upper_bound], weights) 
                # gradient descent 
                weights -= learning_rate * gradients
                
                # update index
                row_idx += batch_size
                
            # compute accuracy with training set  
            y_train_predicted = cls.predict(features, weights)
            training_accuracy = cls.get_accuracy(y_train_predicted, y)
            training_accuracy_hist[epoch] = training_accuracy
            # copmpute accurcy with validation set
            y_val_predicted = cls.predict(X_val, weights, add_bias = True)
            current_val_accuracy = cls.get_accuracy(y_val_predicted, y_val)
            validation_accuracy_hist[epoch] = current_val_accuracy
            
            print('Epoch {}, training accuary:{:.3f}, validateion accuary:{:.3f}'.format(epoch, training_accuracy, current_val_accuracy))
            
            # update saved model
            if best_accuacy < current_val_accuracy:
                best_model_weights = np.copy(weights)
                
        return best_model_weights, training_accuracy_hist, validation_accuracy_hist
    
    @classmethod
    def predict(cls, X: np.ndarray, weights: np.ndarray, add_bias:bool = False) -> np.ndarray:
        """
        predict labels for given X and weights
        args:
            X (np.ndarray): Training features, num_sample by num_features.
            weights (np.ndarray): weights of the model

        Returns:
            [np.ndarray]: predicted labels
        """
        features = X if not add_bias else np.hstack((np.ones((X.shape[0], 1)), X))
        z = np.dot(features, weights)
        predicted_y_value= cls.signmoid(z)
        # predicted_y_valueis in range of 0-1, use 0.5 as boundary
        y_predicted = np.where(predicted_y_value> 0.5, 1, 0)
        
        return y_predicted
    
    @classmethod
    def get_accuracy(cls, y_predicted:np.ndarray, y:np.ndarray) -> float:
        """
        Method to compute accuracy between y and y_predicted
        Args:
            y_predicted (np.ndarray): predicted labels
            y (np.ndarray): ground truth label

        Returns:
            float: [description]
        """
        num_correct = np.sum(y_predicted == y)
        accuracy = num_correct / y_predicted.shape[0]
        return accuracy

def plot_learning_curve(training_accuracy_hist:np.ndarray, validation_accuracy_hist:np.ndarray):
    """
    Method to plot learning curve
    Args:
        training_accuracy_hist ([type]): list of training accurcay
        validation_accuracy_hist ([type]): list of validation accuracy
    """
    plt.figure(figsize=(8, 6))
    plt.plot(range(training_accuracy_hist.shape[0]), training_accuracy_hist, label = 'Training Accuracy')
    plt.plot(range(validation_accuracy_hist.shape[0]), validation_accuracy_hist, label = 'Validation Accuracy')
    
    plt.title('Learning Curve')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    
    plt.legend()
    plt.tight_layout()
    plt.show()
                 
if __name__ == "__main__":
    
    X_train, y_train, X_val, y_val, X_test, y_test = pickle.load( open("data.pkl", "rb"))
    
    # load y labels into np 2d array
    y_train = np.array(y_train).reshape(-1,1)
    y_val = np.array(y_val).reshape(-1,1)
    y_test = np.array(y_test).reshape(-1,1)
        
    num_epochs = 300
    learning_rate = 0.1
    batch_size = 20
    
    model, training_accuracy_hist, validation_accuracy_hist = LogisticRegression.train(X_train, y_train, X_val, y_val, learning_rate = learning_rate, num_epochs = num_epochs,  batch_size = batch_size)
    y_test_predicted = LogisticRegression.predict(X_test, model, add_bias = True)
    y_test_accuracy = LogisticRegression.get_accuracy(y_test_predicted, y_test)
    print('Testing accuracy:{:.3f}'.format(y_test_accuracy))

    plot_learning_curve(training_accuracy_hist, validation_accuracy_hist)