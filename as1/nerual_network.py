
# This is the source code for CMPUT651 AS2
# This work is collaborated with Sarah Davis
from typing import Tuple, Dict

import numpy as np
import pickle
import matplotlib.pyplot as plt

def signmoid_activation(z: np.ndarray) -> np.ndarray:
    """
    Method of sigmoid function
    Args:
        z (np.ndarray): z values

    Returns:
        np.ndarray: output of sigmoid function
    """
    return 1 / (1 + np.exp(-z))


def signmoid_activation_derivative(a: np.ndarray) -> np.ndarray:
    """ 
        Return derivative of a signmoid function
        Note inpnts are activation values
        reference: https://ml-cheatsheet.readthedocs.io/en/latest/activation_functions.html#sigmoid
    Args:
        a (np.ndarray): Activtions from layer

    Returns:
        np.ndarray: derivative
    """
    return a * (1 - a)

def init_weights_and_bias() -> Dict[str,np.ndarray]:
    """ A hard coded method to init weight and bias
    Returns:
        Dict[str,np.ndarray]: A dict of weights, with key W# and B#
    """
    params = {}

    # init weights between input and first layer 200 by 2000 matrix and 200 by 1 martix for bais
    #  2000 weigts and 1 bias per node
    params['W1'] = np.random.uniform(low= -0.5, high = 0.5, size = (200, 2000))
    params['B1'] = np.random.uniform(low= -0.5, high = 0.5, size = (200, 1))

    # init weights between input and second layer 200 by 1 matrix and 1 by 1 martix for bais
    #  200 weigts and 1 bias per node
    params['W2'] =  np.random.uniform(low= -0.5, high = 0.5, size = (1, 200))
    params['B2'] =  np.random.uniform(low= -0.5, high = 0.5, size = (1, 1))
    
    return params


def forward(X: np.ndarray, params: Dict[str,np.ndarray]) -> Dict[str,np.ndarray]:
    """ forward nmethods, return a Dict of activation values, one item per layers 

    Args:
        X (np.ndarray): features
        params (Dict[str,np.ndarray]): a Dict of params values, with key W# and B#

    Returns:
        Dict[str,np.ndarray]: a Dict of activation values, one item per layers 
    """

    forward_results = {}
    # layer 1 -2
    # compute output of each node
    z1 = np.dot(params['W1'], X.T) + params['B1']
    # compute activation
    forward_results['A1'] = signmoid_activation(z1)
    
    # layer 2 - output
    # compute output of each node
    z2 = np.dot(params['W2'], forward_results['A1']) + params['B2']
    # compute activation
    forward_results['A2'] = signmoid_activation(z2)
    return forward_results

def backward(X:np.ndarray, y:np.ndarray, forward_results: Dict[str,np.ndarray], params:Dict[str,np.ndarray]) -> Dict[str,np.ndarray]:
    """backward pass, returns gradient for each layer without bias

    Args:
        X (np.ndarray): features
        y (np.ndarray): ground truth labels
        forward_results (Dict[str,np.ndarray]): foward activation values
        params (Dict[str,np.ndarray]): paramenters
    """
    # reference http://neuralnetworksanddeeplearning.com/chap2.html
    gradients = {}
    # compute layer2 error between output and layer 2
    # we use MSE, compute derivative
    layer2_error = 2 * (forward_results['A2'].T - y)
    # compute the delta for layer 2
    # delta = error * activation derivative
    layer2_delta = layer2_error.T * signmoid_activation_derivative(forward_results['A2'])
    # compute gradients
    gradients['W2'] = np.dot(layer2_delta, forward_results['A1'].T)
    gradients['B2'] = layer2_delta.mean()

    # loss between output and hidden layer
    layer1_error = np.dot(params['W2'].T, layer2_error.T)
    # delta = error * activation derivative
    layer1_delta = layer1_error  * signmoid_activation_derivative(forward_results['A1'])
    # compute gradients
    gradients['W1'] = np.dot(layer1_delta, X)
    gradients['B1'] = layer1_delta.mean()
    return gradients

def predict(X: np.ndarray, params: np.ndarray) -> np.ndarray:
    """ Method that do preidction

    Args:
        X (np.ndarray): features
        params (np.ndarray): params

    Returns:
        [np.ndarray]: predicted labels
    """

    predicted_y_value= forward(X, params)
    # predicted_y_valueis in range of 0-1, use 0.5 as boundary
    y_predicted = np.where(predicted_y_value['A2'].T > 0.5, 1, 0)
    return y_predicted

def train(X: np.ndarray, y: np.ndarray, X_val: np.ndarray, y_val: np.ndarray, learning_rate = 0.1, 
        num_epochs= 300, batch_size = 20) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """
    Method to train the model
    args:
        X (np.ndarray): Training features, num_sample by num_features.
        y (np.ndarray): Training label, num_sample by 1.
        X_val (np.ndarray): Validation features, num_sample by num_features.
        y_val (np.ndarray): Validation label, num_sample by 1.
        layers(list[int]) : list of int , each number indicates dimesion of a layer
        learning_rate (float, optional): learning rate. Defaults to 0.1.
        num_epochs (int, optional): num of epoches. Defaults to 300.
        batch_size (int, optional): size of batch. Defaults to 20.

    Returns:
        Tuple[np.ndarray, np.ndarray, np.ndarray]: weights, list of training accuarcy, and list of validation accuarcy
    """

    params = init_weights_and_bias()

    # init some vals
    best_model_weights = None
    best_accuracy = 0.0
    training_accuracy_hist = np.zeros(num_epochs)
    validation_accuracy_hist = np.zeros(num_epochs)

    for epoch in range(num_epochs):
        row_idx = 0
        while row_idx < X.shape[0]:
            
            upper_bound = row_idx + batch_size if row_idx + \
                batch_size < X.shape[0] else X.shape[0] - 1
            num_samples = upper_bound - row_idx

            # forward pass
            forward_results = forward(X[row_idx: upper_bound], params)
            # backward pass
            gradients = backward(X[row_idx: upper_bound], y[row_idx: upper_bound], forward_results, params)

            # update params vs batch gradients
            params['W2'] -= learning_rate * gradients['W2'] / num_samples
            params['B2'] -= learning_rate * gradients['B2']

            params['W1'] -= learning_rate * gradients['W1'] / num_samples
            params['B1'] -= learning_rate * gradients['B1']

            # update idx
            row_idx += batch_size

        # compute accuracy with training set
        y_train_predicted = predict(X, params)
        training_accuracy = get_accuracy(y_train_predicted, y)
        training_accuracy_hist[epoch] = training_accuracy
        # compute accurcy with validation set
        y_val_predicted = predict(X_val, params)
        current_val_accuracy = get_accuracy(y_val_predicted, y_val)
        validation_accuracy_hist[epoch] = current_val_accuracy

        print('Epoch {}, training accuary:{:.3f}, validateion accuary:{:.3f}'.format(
            epoch, training_accuracy, current_val_accuracy))

        # update saved model
        if best_accuracy < current_val_accuracy:
            best_model_weights = params

    return best_model_weights, training_accuracy_hist, validation_accuracy_hist


def get_accuracy(y_predicted: np.ndarray, y: np.ndarray) -> float:
    """
    Method to compute accuracy between y and y_predicted
    Args:
        y_predicted (np.ndarray): predicted labels
        y (np.ndarray): ground truth label

    Returns:
        float: accuracy
    """
    num_correct = np.sum(y_predicted == y)
    accuracy = num_correct / y_predicted.shape[0]
    return accuracy

def plot_learning_curve(training_accuracy_hist: np.ndarray, validation_accuracy_hist: np.ndarray, filename:str):
    """
    Method to plot learning curve
    Args:
        training_accuracy_hist ([type]): list of training accurcay
        validation_accuracy_hist ([type]): list of validation accuracy
        filename (str): filename for saved figures
    """
    plt.figure(figsize=(8, 6))
    plt.plot(range(training_accuracy_hist.shape[0]), training_accuracy_hist, label='Training Accuracy')
    plt.plot(range(validation_accuracy_hist.shape[0]), validation_accuracy_hist, label='Validation Accuracy')

    plt.title('Learning Curve')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')

    plt.legend()
    plt.tight_layout()
    plt.savefig(filename)

if __name__ == "__main__":

    X_train, y_train, X_val, y_val, X_test, y_test = pickle.load(
        open("data.pkl", "rb"))

    # load y labels into np 2d array
    y_train = np.array(y_train).reshape(-1, 1)
    y_val = np.array(y_val).reshape(-1, 1)
    y_test = np.array(y_test).reshape(-1, 1)

    num_epochs = 300
    learning_rate = 0.1
    batch_size = 20
    print('Epoches:{}, Learning Rate:{}, Batch Size:{}'.format(num_epochs, learning_rate, batch_size))
    model, training_accuracy_hist, validation_accuracy_hist = train(X_train, y_train, X_val, y_val, learning_rate=learning_rate, num_epochs = num_epochs,  batch_size = batch_size)
    
    y_test_predicted = predict(X_test, model)
    y_test_accuracy = get_accuracy(y_test_predicted, y_test)
    print('Testing accuracy:{:.3f}'.format(y_test_accuracy))
    plot_learning_curve(training_accuracy_hist, validation_accuracy_hist, 'nn_learning_curve.png')