from typing import Dict, List, Tuple
from os import listdir
from os.path import isfile, join

import numpy as np
import pickle

def get_files(input_dir:str, shuffle:bool = False) -> List[str]:
    """ 
    Method get files in a directory, return list of path
    Args:
        input_dir (str): path to input samples directory
        shuffle (bool) : bool value, if true shuffe list, otherwise do nothing
    Returns:
        List[str]: a list of input file path in given directory
    """
    input_files = [join(input_dir, f) for f in listdir(input_dir) if isfile(join(input_dir, f))]
    if shuffle:
        np.random.shuffle(input_files)
    return input_files

def read_all_sentences(input_files:List[str]) -> List[str]:
    """
    Method to read all sectences in the list of file path
    Args:
        input_files (List[str]): list of path to input files

    Returns:
        List[str]: A list of sentences, one sentence per item
    """
    all_sentences = []
    for file_path in input_files:
        with open(file_path, "r", encoding="utf-8") as f:
            text = f.readline().strip().lower()
            all_sentences.append(text)
    return all_sentences

def create_data_set(pos_files:List[str], neg_files:List[str], seed:int = None) -> Tuple[np.ndarray, np.ndarray]:
    """
    Method to create data set 
    Args:
        pos_files (List[str]): list of path to positive samples
        neg_files (List[str]): list of path to negative samples
        seed (int): Seed vaue used for random shuffle, no suffle will take place if seed is None

    Returns:
        Tuple[np.ndarray, np.ndarray]]: X, y
    """

    pos_samples = read_all_sentences(pos_files)
    neg_samples = read_all_sentences(neg_files)
    
    X = pos_samples + neg_samples
    y = [1] * len(pos_samples) + [0] * len(neg_samples)
    
    if seed != None:
        np.random.seed(seed)
        np.random.shuffle(X)
        np.random.seed(seed)
        np.random.shuffle(y)
    return X,y

def create_vocab(all_train_sentences:List[str], num_limited:int) -> Tuple[Dict[int, str], Dict[str,int]]:
    """
    Metho to create vocabulary dictionary 
    Args:
        all_train_sentences (List[str]): list of sentences one per item
        num_limited (int): size limitation of dictionary 

    Returns:
        Dict[], Dict[]: id2word, word2id 
    """
    
    # create vocab count dictionary  
    vocab = {}
    for sentence in all_train_sentences:
        for word in sentence.split():
            if word in vocab:
                vocab[word] += 1
            else:
                vocab[word] = 1
    # get sorted vaocab list base on count in reverse order
    sorted_vocab = sorted(vocab.items(), key=lambda x: x[1], reverse=True)
    # create and populate id2word and word2id dictionary
    id2word, word2id = {}, {}
    
    for idx, word_item in enumerate (sorted_vocab):
        if idx == num_limited:
            break    
        word = word_item[0]
        id2word[idx] = word
        word2id[word] = idx
        
    return id2word, word2id 

def create_features_bow(word2id:Dict[str,int], sentences:List[str]) -> np.ndarray:
    """
    Metohd to create features using bag of words
    Args:
        word2id (Dict[str,int]): word and id mapping
        sentences (List[str]): list of sentences
        
    Returns:
        np.ndarray: features representations for inputs
    """
    num_vocab = len(word2id)
    num_input_text = len(sentences)
    features = np.zeros((num_input_text, num_vocab))
    for idx,sentence in enumerate(sentences):
        for word in sentence.split():
            if word in word2id:
                features[idx, word2id[word]] = 1
    return features
        
if __name__ == "__main__":
    
    # define where are the input files    
    input_dir_base = 'aclImdb'
    train_pos_files = get_files(join(input_dir_base,'train','pos'), shuffle= True)
    train_neg_files = get_files(join(input_dir_base,'train','neg'), shuffle= True)
    
    test_pos_files = get_files(join(input_dir_base,'test','pos'))
    test_neg_files = get_files(join(input_dir_base,'test','neg'))
    
    # extract data from dataset
    seed = 314
    val_sample_count = 2500
    
    X_train_text, y_train = create_data_set(train_pos_files[0:-val_sample_count],train_neg_files[0:-val_sample_count], seed)
    X_val_text, y_val = create_data_set(train_pos_files[-val_sample_count:], train_neg_files[-val_sample_count:])
    X_test_text, y_test = create_data_set(test_pos_files, test_neg_files)
    
    # create vacob
    vocab_limit = 2000
    id2word, word2id = create_vocab(X_train_text, vocab_limit)
    
    # save vacob
    pickle.dump((id2word, word2id), open("./aux.pkl", "wb+"))
    
    X_train = create_features_bow(word2id, X_train_text)
    X_val = create_features_bow(word2id, X_val_text)
    X_test = create_features_bow(word2id, X_test_text)

    # save data
    pickle.dump((X_train, y_train, X_val, y_val, X_test, y_test), open("./data.pkl", "wb+"))